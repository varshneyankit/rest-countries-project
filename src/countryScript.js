const API_URL = "https://restcountries.com/v3.1/all";

let API_DATA = [];
let allCountriesData = [];

function populateLoader() {
  let bodyContainer = document.querySelector(".body-container");
  let loader = document.createElement("div");
  let imageElement = document.createElement("img");
  loader.className = "loader w-[100%] flex justify-center align-middle";
  imageElement.className = "inline-flex w-[60px] h-[60px] mt-32";
  imageElement.setAttribute("src", "./images/spinner.gif");
  loader.appendChild(imageElement);
  bodyContainer.appendChild(loader);
}

async function fetchAPIData(API_URL) {
  try {
    const response = await fetch(API_URL);
    const json = await response.json();

    API_DATA = json;
    allCountriesData = json;

    populateCountryData();
    handleEventListeners();
  } catch (error) {
    let loader = document.querySelector(".loader");
    let headingElement = document.createElement("h2");
    loader.innerHTML = "";
    headingElement.className =
      "w-[100%] font-medium lg:text-xl text-lg text-center mt-8";
    headingElement.textContent = "API Error occured!!";
    loader.appendChild(headingElement);
  }
}

function populateBorderCountries(borderCodes) {
  let borderCountriesData = [];

  if (!Array.isArray(borderCodes)) {
    return;
  }

  for (let index = 0; index < borderCodes.length; index++) {
    let borderCode = borderCodes[index];

    API_DATA.find((countryData, index) => {
      if (countryData.cca3 == borderCode) {
        borderCountriesData.push({
          countryName: countryData.name.common,
          index: index,
        });
        return true;
      }
    });
  }

  let containerElement = document.createElement("div");
  containerElement.className = "flex flex-col";

  for (let index = 0; index < borderCountriesData.length; index++) {
    let countryData = borderCountriesData[index];

    let divElement = document.createElement("div");
    let aElement = document.createElement("a");

    aElement.className = "border-country inline-block px-4 py-1 my-2 shadow-md";
    aElement.setAttribute("href", `country.html?index=${countryData.index}`);
    aElement.textContent = countryData.countryName;

    divElement.appendChild(aElement);
    containerElement.appendChild(divElement);
  }

  return containerElement;
}

function populateCountryData() {
  const urlParams = new URLSearchParams(window.location.search);
  const index = urlParams.get("index");

  const countryData = API_DATA[index];

  let countryName = countryData?.name?.common;
  let flagURL = countryData.flags.png;
  let flagAltText = countryData.flags.alt;
  let population = countryData.population.toLocaleString();
  let region = countryData.region;
  let subregion = countryData.subregion;
  let borderCodes = countryData.borders;
  let nativeName = undefined;
  let capital = undefined;
  let topLevelDomain = undefined;
  let currencies = undefined;
  let languages = undefined;

  if (countryData?.name?.nativeName) {
    let nativeNamesData = Object.values(countryData?.name?.nativeName);
    nativeName = nativeNamesData[nativeNamesData.length - 1].common;
  }

  if (Array.isArray(countryData?.capital) && countryData.capital.length > 0) {
    capital = countryData.capital[0];
  }

  if (Array.isArray(countryData?.tld) && countryData.tld.length > 0) {
    topLevelDomain = countryData.tld[0];
  }

  if (typeof countryData.currencies == "object") {
    let currenciesData = Object.values(countryData.currencies);
    currencies = currenciesData.map((currencyData) => currencyData.name);
  }

  if (typeof countryData.languages == "object") {
    languages = Object.values(countryData.languages).sort();
  }

  const bodyContainer = document.querySelector(".body-container");
  const countrySection = document.createElement("section");
  countrySection.className = "country-section";

  countrySection.innerHTML = `<div
          class="country-section-container  max-w-[1200px] mx-auto px-8 py-8"
        >
          <div class="back-button">
            <a href="index.html" class="shadow-md px-8 py-2 inline-flex">
              <svg class="pr-2" width="20px" height="20px" xmlns="http://www.w3.org/2000/svg" id="Outline" viewBox="0 0 24 24" width="512" height="512"><path d="M.88,14.09,4.75,18a1,1,0,0,0,1.42,0h0a1,1,0,0,0,0-1.42L2.61,13H23a1,1,0,0,0,1-1h0a1,1,0,0,0-1-1H2.55L6.17,7.38A1,1,0,0,0,6.17,6h0A1,1,0,0,0,4.75,6L.88,9.85A3,3,0,0,0,.88,14.09Z"/></svg>
              <span>Back</span>
            </a>
          </div>
          <div class="country-block lg:flex justify-between mt-10 ">
            <div class="country-flag w-[100%] lg:w-[42%] h-[220px] lg:h-[300px]  mb-10">
              <img class="w-[100%] h-[100%]" src=${flagURL} alt=${flagAltText} />
            </div>
            <div class="country-data  lg:w-[50%]">
              <div class="country-data-header mb-8">
                <h1 class="text-2xl lg:text-3xl font-semibold">${countryName}</h1>
              </div>
              <div class="country-data-body lg:flex">
                <div class="country-data-section mb-8 lg:w-[50%]">
                  <p class="mb-2">
                    <span class="font-bold">Native Name:</span> <span>${nativeName}</span>
                  </p>
                  <p class="mb-2">
                    <span class="font-bold">Population:</span> <span>${population}</span>
                  </p>
                  <p class="mb-2"><span class="font-bold">Region:</span> <span>${region}</span></p>
                  <p class="mb-2">
                    <span class="font-bold">Sub Region:</span> <span>${subregion}</span>
                  </p>
                  <p class="mb-2"><span class="font-bold">Capital:</span> <span>${capital}</span></p>
                </div>
                <div class="country-data-section mb-8 lg:w-[50%]">
                  <p class="mb-2">
                    <span class="font-bold">Top Level Domain:</span>
                    <span>${topLevelDomain}</span>
                  </p>
                  <p class="mb-2">
                    <span class="font-bold">Currencies:</span> <span>${currencies.join(
                      ", "
                    )}</span>
                  </p>
                  <p class="mb-2">
                    <span class="font-bold">Languages:</span> <span>${languages.join(
                      ", "
                    )}</span>
                  </p>
                </div>
              </div>
              <div class="country-data-footer">
                <p class="font-bold">Border Countries:</p>
              </div>
            </div>
          </div>
        </div>`;

  let loaderElement = document.querySelector(".loader");
  loaderElement.style.display = "none";

  bodyContainer.appendChild(countrySection);

  let countryFooter = document.querySelector(".country-data-footer");
  let borderCountriesElement = populateBorderCountries(borderCodes);
  countryFooter.appendChild(borderCountriesElement);
}

function handleEventListeners() {
  let themeButton = document.querySelector(".dark-btn");
  themeButton.addEventListener("click", (e) => {
    document.body.classList.toggle("light");
    document.body.classList.toggle("dark");
  });
}

populateLoader();
fetchAPIData(API_URL);
