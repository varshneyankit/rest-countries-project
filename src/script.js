const API_URL = "https://restcountries.com/v3.1/all";

let API_DATA = [];
let allCountriesData = [];
let regionCountriesData = [];

function populateLoader() {
  let cardsSectionBody = document.querySelector(".cards-section-body");
  let loader = document.createElement("div");
  let imageElement = document.createElement("img");
  loader.className = "w-[100%] flex justify-center align-middle";
  imageElement.className = "inline-flex w-[60px] h-[60px] mt-32";
  imageElement.setAttribute("src", "./images/spinner.gif");
  loader.appendChild(imageElement);
  cardsSectionBody.appendChild(loader);
}

async function fetchAPIData(API_URL) {
  try {
    const response = await fetch(API_URL);
    const json = await response.json();

    API_DATA = json;
    allCountriesData = json;
    regionCountriesData = json;

    populateFilterByRegion();
    populateCountriesData();
    handleEventListeners();
  } catch (error) {
    let cardsSectionBody = document.querySelector(".cards-section-body");
    let headingElement = document.createElement("h2");
    headingElement.className =
      "w-[100%] font-medium lg:text-xl text-lg text-center mt-8";
    headingElement.textContent = "API Error occured!!";
    cardsSectionBody.innerHTML = "";
    cardsSectionBody.appendChild(headingElement);
  }
}

function populateFilterByRegion() {
  let selectElement = document.getElementById("select-by-region");
  let regions = [];

  API_DATA.forEach((countryData) => {
    let region = countryData.region;
    if (!regions.includes(region)) {
      regions.push(region);
    }
  });

  regions = regions.sort();

  regions.forEach((region) => {
    const optionElement = document.createElement("option");

    optionElement.value = region;
    optionElement.textContent = region;
    selectElement.appendChild(optionElement);
  });
}

function createCountryCard(countryData, index) {
  let countryName = countryData.name.common;
  let flagURL = countryData.flags.png;
  let flagAltText = countryData.flags.alt;
  let population = countryData.population.toLocaleString();
  let region = countryData.region;
  let capital = undefined;

  if (Array.isArray(countryData?.capital) && countryData.capital.length > 0) {
    capital = countryData.capital[0];
  }

  const cardElement = document.createElement("a");

  cardElement.className =
    "card-element w-[80%] lg:w-[250px] shadow-md lg:mr-[60px] mb-8";
  cardElement.setAttribute("href", `country.html?index=${index}`);

  cardElement.innerHTML = `<div class="card-container w-[100%] rounded">
      <div class="image-container h-[160px] rounded">
        <img
          class="h-[100%] w-[100%] rounded"
          src="${flagURL}"
          alt="${flagAltText}"
        />
      </div>
      <div class="country-data-container p-[20px] rounded pb-[40px]">
        <h1 class="country-name font-semibold mb-4 text-xl">${countryName}</h1>
        <p class="population mb-1">
          <span class="font-bold">Population: </span>
          <span>${population}</span>
        </p>
        <p class="region mb-1">
          <span class="font-bold">Region: </span> <span>${region}</span>
        </p>
        <p class="capital mb-1">
          <span class="font-bold">Capital: </span> <span>${capital}</span>
        </p>
      </div>
    </div>`;

  return cardElement;
}

function populateCountriesData() {
  let cardsSection = document.querySelector(".cards-section-body");

  cardsSection.innerHTML = "";

  for (let index = 0; index < allCountriesData.length; index++) {
    let countryCard = createCountryCard(allCountriesData[index], index);
    cardsSection.appendChild(countryCard);
  }
}

function handleEventListeners() {
  let selectElement = document.getElementById("select-by-region");
  let searchInput = document.getElementById("search");
  let themeButton = document.querySelector(".dark-btn");

  selectElement.addEventListener("change", (e) => {
    const region = e.target.value;

    if (region == "all") {
      allCountriesData = API_DATA;
      regionCountriesData = API_DATA;
    } else {
      allCountriesData = API_DATA.filter(
        (countryData) => countryData.region == region
      );
      regionCountriesData = allCountriesData;
    }

    // code for both, input element and select element

    let inputValue = searchInput.value;
    if (inputValue == "") {
      allCountriesData = regionCountriesData;
    } else {
      allCountriesData = regionCountriesData.filter((countryData) =>
        String(countryData.name.common).toLowerCase().includes(inputValue)
      );
    }

    populateCountriesData();
  });

  searchInput.addEventListener("input", (e) => {
    let countryName = String(e.target.value).toLowerCase();

    if (countryName == "") {
      allCountriesData = regionCountriesData;
    } else {
      allCountriesData = regionCountriesData.filter((countryData) =>
        String(countryData.name.common).toLowerCase().includes(countryName)
      );
    }

    populateCountriesData();
  });

  themeButton.addEventListener("click", (e) => {
    document.body.classList.toggle("light");
    document.body.classList.toggle("dark");
  });
}

populateLoader();
fetchAPIData(API_URL);
